package com.em.gui.util;

/**
 * Created by guchong on 6/9/2018.
 */

import com.google.common.io.Resources;
import org.hyperic.sigar.Sigar;

import java.io.File;

/**
 * @author gaohui
 * @date 13-11-27 19:43
 */
public class SigarUtil {

    private static String OS = System.getProperty("os.name").toLowerCase();

    public final static Sigar sigar = initSigar();

    public static boolean isWindows() {

        return (OS.indexOf("win") >= 0);

    }

    public static boolean isMac() {

        return (OS.indexOf("mac") >= 0);

    }

    public static boolean isUnix() {

        return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0 );

    }

    public static boolean isSolaris() {

        return (OS.indexOf("sunos") >= 0);

    }

    public static Sigar initSigar() {
        try {
            String file = Resources.getResource("sigar/.sigar_shellrc").getFile();
            File classPath = new File(file).getParentFile();

            String path = System.getProperty("java.library.path");
            if (isWindows()) {
                path += ";" + classPath.getCanonicalPath();
            } else {
                path += ":" + classPath.getCanonicalPath();
            }
            System.setProperty("java.library.path", path);

            return new Sigar();
        } catch (Exception e) {
            return null;
        }
    }
}
