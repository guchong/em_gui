package com.em.util;

import com.em.model.Product;
import com.em.model.Risk;
import com.em.model.StrategyConfiguration;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.FileUtils;

import java.io.*;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.List;

/**
 * Created by guchong on 5/25/2018.
 */
public class Utils {
    private static final ObjectMapper mapper = new ObjectMapper();

    private static volatile String AUDIT_STRING;

    public static Risk getDefaultRisk(){
        Risk risk = new Risk();
        risk.setFlowControl(300);
        risk.setThreshold(800);
        risk.setTotalNotional(100_000_000);
        risk.setOrdersTotal(40_000);
        risk.setTradeRate(0.2);
        risk.setCancelRate(0.4);
        risk.setRejectRate(0.3);
        return risk;
    }

    public static String getIPString(){
        Enumeration e = null;
        StringBuilder ipString = new StringBuilder();
        try {

            //get IP

            e = NetworkInterface.getNetworkInterfaces();
            while(e.hasMoreElements())
            {
                NetworkInterface n = (NetworkInterface) e.nextElement();
                Enumeration ee = n.getInetAddresses();
                while (ee.hasMoreElements())
                {
                    InetAddress i = (InetAddress) ee.nextElement();
                    if(!i.isLoopbackAddress()){
                        ipString.append(i.toString()).append(",");
                    }
                }
            }
        } catch (SocketException e1) {
            e1.printStackTrace();
        }
        return ipString.toString();
    }

    /**
     * 获取硬盘序列号
     *
     * @param drive
     *            盘符
     * @return
     */
    public static String getHardDiskSN(String drive) {
        String result = "";
        try {
            File file = File.createTempFile("realhowto", ".vbs");
            file.deleteOnExit();
            FileWriter fw = new java.io.FileWriter(file);

            String vbs = "Set objFSO = CreateObject(\"Scripting.FileSystemObject\")\n"
                    + "Set colDrives = objFSO.Drives\n"
                    + "Set objDrive = colDrives.item(\""
                    + drive
                    + "\")\n"
                    + "Wscript.Echo objDrive.SerialNumber"; // see note
            fw.write(vbs);
            fw.close();
            Process p = Runtime.getRuntime().exec(
                    "cscript //NoLogo " + file.getPath());
            BufferedReader input = new BufferedReader(new InputStreamReader(
                    p.getInputStream()));
            String line;
            while ((line = input.readLine()) != null) {
                result += line;
            }
            input.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result.trim();
    }

    /**
     * 获取CPU序列号
     *
     * @return
     */
    public static String getCPUSerial() {
        String result = "";
        try {
            File file = File.createTempFile("tmp", ".vbs");
            file.deleteOnExit();
            FileWriter fw = new java.io.FileWriter(file);
            String vbs = "Set objWMIService = GetObject(\"winmgmts:\\\\.\\root\\cimv2\")\n"
                    + "Set colItems = objWMIService.ExecQuery _ \n"
                    + "   (\"Select * from Win32_Processor\") \n"
                    + "For Each objItem in colItems \n"
                    + "    Wscript.Echo objItem.ProcessorId \n"
                    + "    exit for  ' do the first cpu only! \n" + "Next \n";

            // + "    exit for  \r\n" + "Next";
            fw.write(vbs);
            fw.close();
            Process p = Runtime.getRuntime().exec(
                    "cscript //NoLogo " + file.getPath());
            BufferedReader input = new BufferedReader(new InputStreamReader(
                    p.getInputStream()));
            String line;
            while ((line = input.readLine()) != null) {
                result += line;
            }
            input.close();
            file.delete();
        } catch (Exception e) {
            e.fillInStackTrace();
        }
        if (result.trim().length() < 1 || result == null) {
            result = "无CPU_ID被读取";
        }
        return result.trim();
    }

    public static String getMAC(){
        InetAddress ip;
        try {

            ip = InetAddress.getLocalHost();
            NetworkInterface network = NetworkInterface.getByInetAddress(ip);

            byte[] mac = network.getHardwareAddress();

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < mac.length; i++) {
                sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));
            }
            return sb.toString();

        } catch (UnknownHostException e) {

            e.printStackTrace();

        } catch (SocketException e){

            e.printStackTrace();

        }
        return "Unknow";
    }

    public static String getAuditString(){
        if(AUDIT_STRING==null){
            //AUDIT_STRING = "DDZL-YXSC-IP."+getIPString()+"-MAC."+getMAC()+"-HD."+getHardDiskSN("")+"-PCN."+InetAddress.getLocalHost().getHostName()+"-CPU."+getCPUSerial()+"PI."+"";
        }
        return null;
    }

    public static void flush(String file, Risk risk, List<Product> products){

        File f = new File(file);
        if(!f.exists()){
            File parent = f.getParentFile();
            if(!parent.exists()){
                parent.mkdirs();
            }
        }
        try {
            StrategyConfiguration strategyConfiguration = new StrategyConfiguration(risk,products);
            FileUtils.write(f,mapper.writeValueAsString(strategyConfiguration),"UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }


    }


}
